package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICurrencyConverter extends Remote
{
	public double Convert(String from, String to, double value) throws RemoteException;
}
