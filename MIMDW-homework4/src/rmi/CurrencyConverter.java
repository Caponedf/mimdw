/**
 * 
 */
package rmi;

import java.rmi.*;
import java.rmi.server.*;

/**
 * @author neubauer
 *
 */
public class CurrencyConverter extends UnicastRemoteObject implements ICurrencyConverter {

	private final static double USD = 21;
	private final static double GBP = 31;
	private final static double EUR = 27;

	protected CurrencyConverter() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.ICurrencyConverter#Convert(java.lang.String, java.lang.String,
	 * double)
	 */
	@Override
	public double Convert(String from, String to, double value) throws RemoteException {

		double fromCzkRate = GetCurrencyRate(from);
		double toCzkRate = GetCurrencyRate(to);
		
		double valueInCzk = value*fromCzkRate;
		
		double result = valueInCzk / toCzkRate;
		
		return result;
	}
	
	private static double GetCurrencyRate(String currencyCode)
	{
		double toCzkRate;

		if (currencyCode.toUpperCase().equals("USD"))
			toCzkRate = USD;
		else if (currencyCode.toUpperCase().equals("GBP"))
			toCzkRate = GBP;
		else if (currencyCode.toUpperCase().equals("EUR"))
			toCzkRate = EUR;
		else
			throw new java.lang.UnsupportedOperationException();
		
		return toCzkRate;
		 
	}

}
