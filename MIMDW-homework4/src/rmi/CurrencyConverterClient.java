package rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CurrencyConverterClient {

	

	public static void main(String[] args) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry("localhost", 1098);
        ICurrencyConverter currencyConverter = (ICurrencyConverter)registry.lookup("CurrencyConverter");
        
        try
        {
        	String from = args[0];
        
        String to = args[1];
        
        double value = Double.parseDouble(args[2]);
        
        System.out.println(currencyConverter.Convert(from, to, value));
        }
        catch(NumberFormatException ex)
        {
        	System.err.println("Third argument must be float number.");
        }
        catch(ArrayIndexOutOfBoundsException ex)
        {
        	System.err.println("Argument format is: [from] [to] [value]");
        }
        catch(UnsupportedOperationException ex)
        {
        	System.err.println("Supported currencies are USD, EUR and GBP");
        }
        
        

	}

}
