package sessions;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TicketMachineServlet
 */
public class TicketMachineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String NEW ="NEW";
	private static final String PAYMENT ="PAYMENT";
	private static final String COMPLETED ="COMPLETED";
	
	private enum MachineStatusEnum {
		New(0),
		Payment(1),
		Completed(2);
		
		private final int value;

	    private MachineStatusEnum(int value) {
	        this.value = value;
	    }

	    public int getValue() {
	        return value;
	    }
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TicketMachineServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		PrintWriter out = response.getWriter();
		Object reqStatusAttr = request.getParameter("ms");
		
		MachineStatusEnum requestedNewMachineStatus = null;
		
		if (reqStatusAttr != null)
		{
			requestedNewMachineStatus = searchEnum(MachineStatusEnum.class, reqStatusAttr.toString());
			
			if (requestedNewMachineStatus == null)
			{
				out.println("ERROR: " + reqStatusAttr.toString() + " is not regular machine state.");
				return;
			}
		}
		
		HttpSession	session	= request.getSession();
	
		
		Object ticketMachineAttr = session.getAttribute("TicketMachine");
		MachineStatusEnum machineStatus = ticketMachineAttr == null 
				? MachineStatusEnum.New 
				: searchEnum(MachineStatusEnum.class, ticketMachineAttr.toString());
		
		out.println("Current machine status is: " + machineStatus);
		
		if (requestedNewMachineStatus != null)
		{
			if (machineStatus.getValue() +1 != requestedNewMachineStatus.getValue())
			{
				out.println(
						"ERROR: Ticket machine cannot translate from " 
								+ machineStatus 
								+ " to " 
								+ requestedNewMachineStatus );
			return;	
			}
			
			session.setAttribute("TicketMachine", requestedNewMachineStatus);
			out.println("\nTicket machine is now in state: " + requestedNewMachineStatus);
			
			if (requestedNewMachineStatus.getValue() == 2)
				session.invalidate();
		}
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public static <T extends Enum<?>> T searchEnum(Class<T> enumeration,
	        String search) {
	    for (T each : enumeration.getEnumConstants()) {
	        if (each.name().compareToIgnoreCase(search) == 0) {
	            return each;
	        }
	    }
	    return null;
	}

}
