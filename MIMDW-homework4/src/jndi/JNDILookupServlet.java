package jndi;

import java.io.*;

import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Servlet implementation class JNDILookupServlet
 */
public class JNDILookupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public JNDILookupServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection conn;

		String driver = "com.mysql.jdbc.Driver";
		try 
		{
			Class.forName(driver).newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://snack.fit.cvut.cz:3306/mimdw", "mimdw", "mimdw");

			PrintWriter out = response.getWriter();

			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM test ");

			out.println("<html><body>");
			out.println("<table border=\"1\" style=\"border-collapse: collapse\"><tr><th>Test</th><th>Foobar</th></tr>");
			while (rs.next()) 
			{
				out.print("<tr>");
				out.print("<td>");
				out.print(rs.getString("test"));
				out.print("</td>");
				out.print("<td>");
				out.print(rs.getString("foobar"));
				out.print("</td>");
				out.print("</tr>");
				
			}
			out.println("</table></html></body>");
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
