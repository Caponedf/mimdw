package Services;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import DO.Contact;
import DataManagers.ContactsManager;
import DataManagers.IDataManager;

@WebService
public class ContactsService {

	IDataManager<Contact> dataManager;
	 	public ContactsService()
	 	{
	 		dataManager = new ContactsManager();
	 	}
	 
	    public List<Contact> getContacts(){
	        return dataManager.getAllItems();
	    }
	 
	    public void addContact(Contact contact){
	        dataManager.Add(contact);
	    }
	 
	    public Contact get(String username){
	        Contact result = null;
	        for(Contact c: dataManager.getAllItems()){
	            if(c.getName().equals(username)){
	                result = c;
	                break;
	            }
	        }
	        return result;
	    }
	 
	    public void removeContactByUsername(String username){
	        for(Contact c: dataManager.getAllItems()){
	            if(c.getName().equals(username)){
	                dataManager.Delete(c);
	                break;
	            }
	        }
	    }
}
