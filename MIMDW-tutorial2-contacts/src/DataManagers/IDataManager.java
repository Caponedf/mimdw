package DataManagers;

import java.util.List;

import DO.IEntity;

public interface IDataManager<TEntity extends IEntity> 
{
	List<TEntity> getAllItems();
	
	TEntity getItemById(int id);
	
	void Add(TEntity item);
	
	void Delete(TEntity item);
}
