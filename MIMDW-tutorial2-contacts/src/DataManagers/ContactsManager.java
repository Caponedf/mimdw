package DataManagers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import DO.Contact;

public class ContactsManager extends DataManagerBase<Contact> 
{

	@Override
	protected HashMap<Integer,Contact> InitItems()
	{
		HashMap<Integer,Contact> res = new HashMap<Integer,Contact>();
		
		res.put(1,new Contact(1,"Tomas", "tomas@mail.cz"));
		res.put(2,new Contact(2,"Peter", "peter@mail.cz"));
		res.put(3,new Contact(3,"Martin", "martin@mail.cz"));
		
		return res;
	}

}
