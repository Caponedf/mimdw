package DO;

public class Contact implements IEntity
{
	private int id;
	
	private String name;
	private String mail;
		
	
	public Contact()
	{
		
	}
	
	public Contact(int id, String name, String mail)
	{
		this.id = id;
		this.name = name;
		this.mail = mail;
	}
	
	

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getMail()
	{
		return mail;
	}
	public void setMail(String mail)
	{
		this.mail = mail;
	}
}


