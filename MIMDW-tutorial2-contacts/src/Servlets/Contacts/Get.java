package Servlets.Contacts;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import DO.Contact;
import DataManagers.ContactsManager;
import DataManagers.IDataManager;

/**
 * Servlet implementation class Get
 */
public class Get extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Get() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		
		response.setContentType("application/xml");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		String contactIdParameter = request.getParameter("id");
		int contactId; 
		try
		{
			contactId = Integer.parseInt(contactIdParameter);
		}
		catch(NumberFormatException ex)
		{
			out.append("ERROR: ID parameter is not in correct format.");
			return;
			
		}
		
		IDataManager<Contact> dataManager = new ContactsManager();
		
		Contact contactDO = dataManager.getItemById(contactId);
		
		Document dom;
		Element c = null;
	    Element e = null;
	    

	    // instance of a DocumentBuilderFactory
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    try {
	        // use factory to get an instance of document builder
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        // create instance of DOM
	        dom = db.newDocument();

	        // create the root element
	        Element rootEle = dom.createElement("Contacts");

	        // create data elements and place them under root
	        c = dom.createElement("Contact");
	        
	        e = dom.createElement("Name");
	        e.appendChild(dom.createTextNode(contactDO.getName()));
	        c.appendChild(e);
	        
	        e = dom.createElement("Email");
	        e.appendChild(dom.createTextNode(contactDO.getMail()));
	        c.appendChild(e);
	        
	        rootEle.appendChild(c);

	        
	        dom.appendChild(rootEle);

	        try {
	            Transformer tr = TransformerFactory.newInstance().newTransformer();
	            tr.setOutputProperty(OutputKeys.INDENT, "yes");
	            tr.setOutputProperty(OutputKeys.METHOD, "xml");
	            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
	            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	            StringWriter writer = new StringWriter();
	            tr.transform(new DOMSource(dom), new StreamResult(writer));
	            String output = writer.getBuffer().toString();
	            
	            //out.append(output);
	            out.print(output);

	        } catch (TransformerException te) {
	            System.out.println(te.getMessage());
	        }
	    } catch (ParserConfigurationException pce) {
	        System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
	    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
