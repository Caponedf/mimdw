package test;

import java.util.Date;

import javax.jws.WebService;

import weblogic.rmi.extensions.NotImplementedException;

//import sun.reflect.generics.reflectiveObjects.NotImplementedException;


@WebService
public class SimpleService 
{

	
	
	public Date getCurrentDate() 
	{
		return new Date();
		
	}

	public String stringToUpper(String str)
	{
		if (str == null)
			return null;
		
		return str.toUpperCase();
	}

	public double sqrt(double x) 
	{
		return Math.sqrt(x);
	}
	
	public double calculate(double a, double b, MathOperation operation){
		
		switch(operation)
		{
		case Add:
			return a+b;
	
		case Subtracts:
		return a-b;
		default:
			throw new NotImplementedException();
		}
	}
	
}
