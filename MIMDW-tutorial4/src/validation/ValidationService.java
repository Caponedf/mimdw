package validation;

import javax.jws.WebService;

@WebService
public class ValidationService {


	private static final String USERNAME = "neubatom";
	private static final String EMAIL_DOMAIN = "@fit.cvut.cz";
	
	public Boolean usernameValidator(String username)
	{
		return username.toLowerCase().trim().equals(USERNAME);
	}
	
	public Boolean emailValidator(String email)
	{
		return email.toLowerCase().trim().equals(USERNAME + EMAIL_DOMAIN )||
				email.toLowerCase().trim().matches("\\b@fit.cvut.cz{1}");
	}
	
	public Boolean cardValidator(String cardNo)
	{
		return cardNo.toLowerCase().trim().matches("\\d{4}-{1}\\d{4}-{1}\\d{4}-{1}\\d{4}");
	}
}
