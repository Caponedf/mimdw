package DataManagers;

import java.util.HashMap;

import DO.BankAccount;

public class BankAccountManager extends DataManagerBase<BankAccount> {

	@Override
	protected HashMap<Integer, BankAccount> InitItems() {
		HashMap<Integer,BankAccount> res = new HashMap<Integer,BankAccount>();
		
		res.put(1,new BankAccount(1,150000.0d));
		res.put(2,new BankAccount(2,500000.0d));
		res.put(3,new BankAccount(3,450000.0d));
		
		return res;
	}

	
}
