package DO;

public class BankAccount implements IEntity {

	private int id;
	private double balance;
	
	public BankAccount(int id, double d) {
		super();
		this.id = id;
		this.balance = d;
	}
	
	public BankAccount() {
		super();
	}
	

	

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}

}
