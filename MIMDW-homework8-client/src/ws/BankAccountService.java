/**
 * BankAccountService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface BankAccountService extends java.rmi.Remote {
    public ws.BankAccount getBankAccount(int arg0) throws java.rmi.RemoteException, ws.Exception;
    public java.lang.Boolean validateAccountExistence(int arg0) throws java.rmi.RemoteException, ws.Exception;
    public java.lang.Boolean validateAccountBalance(int arg0, double arg1) throws java.rmi.RemoteException, ws.Exception;
    public void changeBallance(int arg0, double arg1) throws java.rmi.RemoteException, ws.Exception;
}
