package ws;

public class BankAccountServiceProxy implements ws.BankAccountService {
  private String _endpoint = null;
  private ws.BankAccountService bankAccountService = null;
  
  public BankAccountServiceProxy() {
    _initBankAccountServiceProxy();
  }
  
  public BankAccountServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initBankAccountServiceProxy();
  }
  
  private void _initBankAccountServiceProxy() {
    try {
      bankAccountService = (new ws.BankAccountServiceServiceLocator()).getBankAccountServicePort();
      if (bankAccountService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bankAccountService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bankAccountService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bankAccountService != null)
      ((javax.xml.rpc.Stub)bankAccountService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.BankAccountService getBankAccountService() {
    if (bankAccountService == null)
      _initBankAccountServiceProxy();
    return bankAccountService;
  }
  
  public ws.BankAccount getBankAccount(int arg0) throws java.rmi.RemoteException, ws.Exception{
    if (bankAccountService == null)
      _initBankAccountServiceProxy();
    return bankAccountService.getBankAccount(arg0);
  }
  
  public java.lang.Boolean validateAccountExistence(int arg0) throws java.rmi.RemoteException, ws.Exception{
    if (bankAccountService == null)
      _initBankAccountServiceProxy();
    return bankAccountService.validateAccountExistence(arg0);
  }
  
  public java.lang.Boolean validateAccountBalance(int arg0, double arg1) throws java.rmi.RemoteException, ws.Exception{
    if (bankAccountService == null)
      _initBankAccountServiceProxy();
    return bankAccountService.validateAccountBalance(arg0,arg1);
  }
  
  public void changeBallance(int arg0, double arg1) throws java.rmi.RemoteException, ws.Exception{
    if (bankAccountService == null)
      _initBankAccountServiceProxy();
    bankAccountService.changeBallance(arg0, arg1);
  }
  
  
}