package ws;

import java.rmi.RemoteException;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class BankTransferService {
	
	@WebMethod
	public Boolean Transfer(int from, int to, double amount) throws Exception, RemoteException
	{
		BankAccountServiceProxy client = new BankAccountServiceProxy();
		
		if (!client.validateAccountExistence(from) || !client.validateAccountExistence(to))
			return false;
		
		if (!client.validateAccountBalance(from, amount))
			return false;
		
		BankAccount fromAccount = client.getBankAccount(from);
		BankAccount toAccount = client.getBankAccount(to);
		client.changeBallance(fromAccount.getId(), -amount);
		client.changeBallance(toAccount.getId(), amount);
		
		return true;
		
	}
}
