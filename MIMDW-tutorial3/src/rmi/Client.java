

package rmi;
 
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
 
public class Client {
 
    public static void main(String[] args) throws Exception{
        Registry registry = LocateRegistry.getRegistry("localhost", 1099);
        ICalculator calculator = (ICalculator)registry.lookup("Hello");
        System.out.println(calculator.add(5,3));
    }
 
}

