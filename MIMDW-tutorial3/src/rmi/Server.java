package rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

	public static void main(String[] args) {
        try {
            /*
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }
            */
            Registry registry = LocateRegistry.createRegistry(1099);
 
            Calculator server = new Calculator();
            registry.bind("Hello", server);
 
            System.out.println("Server started...");
 
        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }
 
    }
}

