package rmi;


import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;




public class Calculator extends UnicastRemoteObject implements ICalculator, Serializable {

	protected Calculator() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public double add(double a, double b) throws RemoteException{
		return a + b;
	}

	@Override
	public double divide(double a, double divider)throws RemoteException {
		return a / divider;
	}
	
	

}
