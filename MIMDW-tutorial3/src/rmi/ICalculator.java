package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalculator extends Remote
{
	public double add(double a, double b) throws RemoteException;
	
	public double divide(double a, double divider)throws RemoteException;
}
