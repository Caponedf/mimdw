package DataManagers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import DO.IEntity;

public abstract class DataManagerBase<TEntity extends IEntity> implements IDataManager<TEntity>
{
	private HashMap<Integer,TEntity> storage;

	@Override
	public TEntity getItemById(int id) throws Exception {
		
		if (!storage.containsKey(id))
			throw new Exception("Item is not in DB.");
		
		return storage.get(id);
	}
	
	protected DataManagerBase()
	{
		storage = InitItems();
	}
	
	@Override
	public List<TEntity> getAllItems()
	{
		return new ArrayList<TEntity>(storage.values());
	}

	@Override
	public void Add(TEntity item)
	{
		storage.put(item.getId(),item);
	}

	@Override
	public void Delete(TEntity item) throws Exception
	{
		if (!storage.containsKey(item.getId()))
			throw new Exception("Item is not in DB.");
		
		storage.remove(item.getId());
	}
	
	@Override
	public void Update(TEntity item) throws Exception
	{
		Delete(item);
		Add(item);
	}
	protected abstract HashMap<Integer,TEntity> InitItems();
}
