package DataManagers;

import java.util.HashMap;

import DO.OrderDO;

public class OrderManager extends DataManagerBase<OrderDO> {

	@Override
	protected HashMap<Integer, OrderDO> InitItems() {
		HashMap<Integer,OrderDO> res = new HashMap<Integer,OrderDO>();
		
		res.put(1,new OrderDO(1,"Tomas", "tomas@mail.cz"));
		res.put(2,new OrderDO(2,"Peter", "peter@mail.cz"));
		res.put(3,new OrderDO(3,"Martin", "martin@mail.cz"));
		
		return res;
	}
}
