package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import DO.OrderDO;
import DataManagers.IDataManager;
import DataManagers.OrderManager;

@WebService
public class OrderService 
{
	private IDataManager<OrderDO> dataManager;
	
	public OrderService() {
		 
		dataManager = new OrderManager();
		
	}

	@WebMethod
	public List<OrderDO> getAllItems() {
		return dataManager.getAllItems();
	}

	@WebMethod
	public OrderDO getItemById(int id) throws Exception {
		return dataManager.getItemById(id);
	}

	@WebMethod
	public void Add(OrderDO item) {
		dataManager.Add(item);
		
	}

	@WebMethod
	public void Delete(OrderDO item) throws Exception {
		dataManager.Delete(item);
		
	}
	
	@WebMethod
	public void Update(OrderDO item) throws Exception {
		dataManager.Update(item);
		
	}
}
