package DO;

public class OrderDO implements IEntity{

	private int id;
	
	private String name;
	
	private String purpose;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public void setId(int id) {
		this.id = id;
	}

	public OrderDO(int id, String name, String purpose) {
		super();
		this.id = id;
		this.name = name;
		this.purpose = purpose;
	}
	
	public OrderDO() {
		super();

	}

	@Override
	public int getId() {
		return id;
	}

}
