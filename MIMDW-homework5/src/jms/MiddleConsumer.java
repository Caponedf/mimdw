package jms;

import javax.jms.Message;
import javax.jms.MessageListener;

public class MiddleConsumer extends JMSConsumer implements MessageListener 
{

	JMSProducer middleProducer;
	
	public MiddleConsumer(){
		
		middleProducer = new JMSProducer();
		
	}
	 
	
	@Override
	public void messageReceived(String msgText) {
		
		try {
			System.out.println("Middle consumer received: " + msgText);
			middleProducer.send("jms/mdw-queue-2", msgText);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception {
        // input arguments
        String queueName = "jms/mdw-queue" ;
 
        // create the producer object and receive the message
        JMSConsumer consumer = new MiddleConsumer();
        consumer.receive(queueName);
    }

}
