package jms;

import javax.jms.MessageListener;

public class MessageConsumer extends JMSConsumer implements MessageListener 
{

	@Override
	public void messageReceived(String msgText) {
		
		System.out.println("Message Received on the end: " + msgText);
		
	}

	public static void main(String[] args) throws Exception {
        // input arguments
        String queueName = "jms/mdw-queue-2" ;
 
        // create the producer object and receive the message
        JMSConsumer consumer = new MessageConsumer();
        consumer.receive(queueName);
    }
}
