package jms;

public class MessageProducer {

	public static void main(String[] args) throws Exception {
        // input arguments
        String msg = "Hello" ;
        String queueName = "jms/mdw-queue" ;

        // create the producer object and send the message
        JMSProducer producer = new JMSProducer();
        producer.send(queueName, msg);
    }
}
